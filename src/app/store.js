import { configureStore } from "@reduxjs/toolkit";

// Acciones
import usuarioReducer from "../features/usuarioSlice";
import mobiliarioReducer from "../features/mobiliarioSlice";

export const store = configureStore({
  reducer: {
    usuarios: usuarioReducer,
    mobiliarios: mobiliarioReducer,
  },
});
