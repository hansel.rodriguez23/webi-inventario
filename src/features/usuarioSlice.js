import { createSlice } from "@reduxjs/toolkit";

export const usuarioSlice = createSlice({
  name: "usuarios",
  initialState: [],
  reducers: {
    addUsuarios: (state, action) => {
      return action.payload;
    },
    addUsuario: (state, action) => {
      state.push(action.payload);
    },
    editUsuario: (state, action) => {
      const index = state.findIndex((usuario) => usuario.id === action.payload.idusuario);
      state[index] = action.payload;
    },
    deleteUsuario: (state, action) => {
      const index = state.findIndex((usuario) => usuario.id === action.payload);
      state.splice(index, 1);
    },
  },
});

export const { addUsuarios, addUsuario, editUsuario, deleteUsuario } = usuarioSlice.actions;

export default usuarioSlice.reducer;
