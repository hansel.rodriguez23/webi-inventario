import { createSlice } from "@reduxjs/toolkit";

export const mobiliarioSlice = createSlice({
  name: "mobiliarios",
  initialState: [],
  reducers: {
    addMobiliarios: (state, action) => {
      return action.payload;
    },
    addMobiliario: (state, action) => {
      state.push(action.payload);
    },
    editMobiliario: (state, action) => {
      const index = state.findIndex(
        (mobiliario) => mobiliario.idmobiliario === action.payload.idmobiliario
      );
      state[index] = action.payload;
    },
    deleteMobiliario: (state, action) => {
      const index = state.findIndex((mobiliario) => mobiliario.idmobiliario === action.payload);
      state.splice(index, 1);
    },
  },
});

export const { addMobiliarios, addMobiliario, editMobiliario, deleteMobiliario } =
  mobiliarioSlice.actions;

export default mobiliarioSlice.reducer;
