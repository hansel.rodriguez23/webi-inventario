import { inventoryFormValidations } from "./validations";

export const inventoryInputsObject = [
  {
    id: "nombre_mobiliario",
    name: "nombre_mobiliario",
    type: "text",
    placeholder: "Nombre del mobiliario",
    icon: "fas fa-box",
    validationMessage: {
      isNotValid: "Mínimo 3 y máximo 25 caracteres",
      isValid: "Formato válido",
    },
    validationRegex: inventoryFormValidations.nombreMobiliario,
  },
  {
    id: "numero_serie",
    name: "numero_serie",
    type: "text",
    placeholder: "Número de Serie",
    icon: "fas fa-barcode",
    validationMessage: {
      isNotValid: "Números, letras, guiones, slash y máximo 15 caracteres",
      isValid: "Formato válido",
    },
    validationRegex: inventoryFormValidations.numeroSerie,
  },
  {
    id: "facultad",
    name: "facultad",
    type: "select",
    label: "Destino",
    labelClassName: "inventario-label",
    options: [
      {
        value: "",
        text: "Selecciona una facultad",
      },
      {
        value: "Ciencias Informaticas",
        text: "Ciencias Informaticas",
      },
      {
        value: "Trabajo Social",
        text: "Trabajo Social",
      },
      {
        value: "Eduacion Inicial",
        text: "Eduacion Inicial",
      },
    ],
    validationMessage: {
      isNotValid: "El destino no ha sido seleccionado",
      isValid: "Destino seleccionado",
    },
    validationRegex: inventoryFormValidations.facultad,
  },
  {
    id: "estado",
    name: "estado",
    type: "select",
    label: "Estado",
    labelClassName: "inventario-label",
    options: [
      {
        value: "",
        text: "Selecciona un estado",
      },
      {
        value: "Nuevo",
        text: "Nuevo",
      },
      {
        value: "Usado",
        text: "Usado",
      },
    ],
    validationMessage: {
      isNotValid: "El estado no ha sido seleccionado",
      isValid: "Estado seleccionado",
    },
    validationRegex: inventoryFormValidations.estado,
  },
];
