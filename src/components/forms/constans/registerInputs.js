import { registerFormValidations } from "./validations";

export const registerInputsObject = [
  {
    id: "username",
    name: "username",
    type: "text",
    placeholder: "Usuario",
    icon: "fas fa-user",
    validationMessage: {
      isNotValid: "Mínimo 3 y máximo 25 caracteres",
      isValid: "Formato válido",
    },
    validationRegex: registerFormValidations.username,
  },
  {
    id: "email",
    name: "email",
    type: "text",
    placeholder: "Correo electronico",
    icon: "fas fa-envelope",
    validationMessage: {
      isNotValid: "Por favor, usa el formato: usuario@example.com",
      isValid: "Formato válido",
    },
    validationRegex: registerFormValidations.email,
  },
  {
    id: "password",
    name: "password",
    type: "password",
    placeholder: "Contraseña",
    icon: "fas fa-key",
    validationMessage: {
      isNotValid: "Una mayúscula, un número y al menos 8 caracteres",
      isValid: "Formato válido",
    },
    validationRegex: registerFormValidations.password,
  },
  {
    id: "rol",
    name: "rol",
    type: "select",
    label: "¿Cuál es tu rol en la ULEAM?",
    options: [
      {
        value: "",
        text: "Selecciona tu rol",
      },
      {
        value: "empleador",
        text: "Empleador",
      },
      {
        value: "estudiante",
        text: "Estudiante",
      },
    ],
    validationMessage: {
      isNotValid: "Seleccione un rol",
      isValid: "Rol seleccionado",
    },
    validationRegex: registerFormValidations.rol,
  },
];
