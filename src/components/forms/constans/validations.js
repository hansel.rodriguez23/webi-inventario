// selectRegex --> El valor de entrada no debe ser una cadena vacía ni solo espacios en blanco
// basicTextRegex --> Mínimo 3 caracteres, máximo 25, solo letras, números, guiones y guiones bajos
// passwordRegex --> Mínimo 8 caracteres, al menos una letra mayúscula, una minúscula y un número
// passwordRegex --> además caracteres especiales

const selectRegex = /^(?!\s*$).+/;
const basicTextRegex = /^[a-zA-Z0-9_-]{3,25}$/;
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\w\W]{8,}$/;

// email    --> Ejemplo de formato permitido: usuario@example.com

export const registerFormValidations = {
  username: basicTextRegex,
  email: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
  password: passwordRegex,
  confirmPassword: passwordRegex,
  rol: selectRegex,
};

export const loginFormValidations = {
  username: registerFormValidations.username,
  password: registerFormValidations.password,
};

// nombreMobiliario --> Lo mismo que basicTextRegex pero se permite espacios en blanco
// numeroSerie --> Solo números, letras, guiones y slash con un máximo de 15 caracteres

export const inventoryFormValidations = {
  nombreMobiliario: /^[a-zA-Z0-9_-\s]{3,25}$/,
  numeroSerie: /^[a-zA-Z0-9-\/]{1,15}$/,
  facultad: selectRegex,
  estado: selectRegex,
};

export default {
  loginFormValidations,
  registerFormValidations,
  inventoryFormValidations,
};
