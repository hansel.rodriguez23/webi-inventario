import { loginFormValidations } from "./validations";

export const loginInputsObject = [
  {
    id: "username",
    name: "username",
    type: "text",
    placeholder: "Usuario",
    icon: "fas fa-envelope",
    validationMessage: {
      isNotValid: "Mínimo 3 y máximo 25 caracteres",
      isValid: "Formato válido",
    },
    validationRegex: loginFormValidations.username,
  },
  {
    id: "password",
    name: "password",
    type: "password",
    placeholder: "Contraseña",
    icon: "fas fa-key",
    validationMessage: {
      isNotValid: "Una mayúscula, un número y al menos 8 caracteres",
      isValid: "Formato válido",
    },
    validationRegex: loginFormValidations.password,
  },
];
