import React, { useState } from "react";

// Utils
import { generateInputClassName } from "./utils/formUtils";

const HtmlFormElement = ({
  inputDetails,
  onChange,
  isFormSubmitted,
  inputValidityStatus,
  initialRenderStatus,
  onFirstRenderStatusChange,
}) => {
  // Obtener los detalles del input
  const { id, icon, type, name, placeholder, validationRegex } = inputDetails;
  // Obtener el valor del label si existe
  const label = inputDetails.label ? inputDetails.label : "";
  // Obtener el className del label del select si este existe sino usar el valor por defecto
  const labelClassName = inputDetails.labelClassName
    ? inputDetails.labelClassName
    : "text-align-center";
  // Obtener el valor de las opciones del select si existen
  const options = inputDetails.options ? inputDetails.options : [];
  // Obtener los mensajes de validación del input
  const { isNotValid, isValid } = inputDetails.validationMessage;

  // Propiedades internas del componente
  const [inputValue, setInputValue] = useState("");
  const [isInputValid, setIsInputValid] = useState(
    isFormSubmitted === true ? inputValidityStatus : false
  );
  // Para mostrar el contenido del mensaje de validación del input
  const inputClassName = generateInputClassName(initialRenderStatus, isInputValid);

  // generateInputClassName(initialRenderStatus, isInputValid);

  const handleChange = (event) => {
    // Ejecutar la función onChange del componente padre
    onChange(event);
    // Cambiar el estado de la primera renderización del input
    initialRenderStatus === true && onFirstRenderStatusChange(name, false);
    // Actualizar el estado del componente
    setInputValue(event.target.value);
    // Validar el input
    handleValidateInput(event.target.value);
  };

  const handleValidateInput = (inputValue) => {
    // Validar el input
    const isValid = validationRegex.test(inputValue);
    // Actualizar el estado del input
    setIsInputValid(isValid);
  };

  // validar si es texto o password o select
  return (
    <>
      {type !== "select" ? (
        <div className='input-contenedor'>
          <i id={`${id}-icon`} className={`${icon} input-icon`}></i>
          <input
            type={type}
            id={id}
            name={name}
            autoComplete='off'
            placeholder={placeholder}
            onChange={handleChange}
            value={inputValue}
          />
        </div>
      ) : (
        <div className='select-contenedor'>
          <label htmlFor={id} className={labelClassName}>
            {label}
          </label>
          <select id={id} name={name} onChange={handleChange}>
            {options.map((option, index) => (
              <option key={option.value} value={option.value} hidden={index === 0 ? true : false}>
                {option.text}
              </option>
            ))}
          </select>
        </div>
      )}
      <div className={inputClassName}>{isInputValid === true ? isValid : isNotValid}</div>
    </>
  );
};

export default HtmlFormElement;
