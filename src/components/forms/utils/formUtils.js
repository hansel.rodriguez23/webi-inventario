import { v4 as generateUniqueId } from "uuid";

// Función para obtener los datos del formulario
export const getFormData = (currentForm) => {
  // Obtener los datos del formulario
  const formData = Object.fromEntries(new FormData(currentForm));
  // Retornar los datos
  return formData;
};

// Generar el className para la validación de un input
export const generateInputClassName = (isFirstRender, isValid) => {
  // Si es el primer render, no se muestra nada
  if (isFirstRender) return "d-none";
  // Si no es el primer render, se muestra la clase correspondiente
  return isValid ? "input-valido d-block" : "input-no-valido d-block";
};

// Actualizar el estado de los inputs cuando se renderiza el componente
export const onInputRenderStatusChange = (inputName, currentInputValuesState, inputStatus) => {
  // Crear una copia del estado actual
  const newInputValues = [...currentInputValuesState];
  // Buscar el campo de entrada con el nombre dado y actualizar su estado
  const inputIndex = newInputValues.findIndex((input) => input.name === inputName);
  // Actualizar el estado del input
  newInputValues[inputIndex] = { ...newInputValues[inputIndex], isFirstRender: inputStatus };
  // Retornar el nuevo estado de los inputs
  return newInputValues;
};

// Actualizar el estado del componente dependiendo del input
export const onInputChanged = (evt, currentInputValuesState) => {
  // Obtener el nombre y el valor del input
  const { name, value } = evt.target;
  // Crear una copia del estado actual
  const newInputValues = [...currentInputValuesState];
  // Buscar el campo de entrada con el nombre dado y actualizar su valor
  const inputIndex = newInputValues.findIndex((input) => input.name === name);
  // Actualizar el valor del input
  newInputValues[inputIndex] = { ...newInputValues[inputIndex], value };
  // Retornar el nuevo estado de los inputs
  return newInputValues;
};

// Validar los datos del formulario
export const onFormSubmitted = (
  evt,
  formRef,
  currentInputValuesState,
  formInputsObject,
  generateUUID = false
) => {
  evt.preventDefault();
  // Se obtienen los datos del formulario
  const formData = getFormData(formRef.current);
  // Validar los datos del formulario
  const newInputValues = Object.entries(formData).map(([inputName, inputValue]) => {
    // Buscar el id del input con el nombre dado en el envío del formulario
    const inputId = formInputsObject.findIndex((loginInput) => loginInput.name === inputName);
    // Validar el input
    const isValid = formInputsObject[inputId].validationRegex.test(inputValue);
    // Obtener el estado actual del input
    const currentInputValueState = currentInputValuesState.find(
      (input) => input.name === inputName
    );
    // Actualizar el estado del input
    return {
      ...currentInputValueState,
      value: inputValue,
      isValid,
      isFirstRender: false,
    };
  });
  // Verificar si el formulario es válido
  const isFormValid = newInputValues.every((input) => input.isValid);

  // Crear una copia del objeto con los datos del formulario
  const newFormData = { ...formData };
  // Si el formulario es válido, se agrega el id al la copia del formulario
  if (isFormValid && generateUUID) newFormData.id = generateUniqueId();

  // Retornar los nuevos estados de los inputs
  return [newInputValues, isFormValid, newFormData];
};
