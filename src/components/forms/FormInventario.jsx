import React, { useState, useRef } from "react";
import { useDispatch } from "react-redux";

// Components
import HtmlFormElement from "./HtmlFormElement";

// Hooks
import useLocalStorage from "../hooks/useLocalStorage";
import useSweetAlert from "../hooks/useSweetAlert";

// Utils
import { onInputRenderStatusChange, onInputChanged, onFormSubmitted } from "./utils/formUtils";

// Constans
import { inventoryInputsObject } from "./constans/inventoryInputs";

// Actions
import { addMobiliario } from "../../features/mobiliarioSlice";

export const FormInventario = () => {
  // Referencias
  const formRef = useRef(null);
  // Estados
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [inputValues, setInputValues] = useState(
    inventoryInputsObject.map((input) => ({
      name: input.name,
      value: "",
      isValid: false,
      isFirstRender: true,
    }))
  );
  // Hooks
  const dispatch = useDispatch();
  const { showSuccessAlert, showErrorAlert } = useSweetAlert();
  const [getLocalStorageData, setLocalStorageData] = useLocalStorage("mobiliarios");

  const updateInitialInputRenderFlag = (inputName, inputStatus) => {
    // Actualizar el estado de los inputs
    const newInputValues = onInputRenderStatusChange(inputName, inputValues, inputStatus);
    // Actualizar el estado del componente
    setInputValues(newInputValues);
  };

  // Actualizar el estado del componente dependiendo del input que cambió
  const handleChange = (event) => {
    // Actualizar el estado de los inputs
    const newInputValues = onInputChanged(event, inputValues);
    // Actualizar el estado del componente
    setInputValues(newInputValues);
  };

  // Manejar el evento submit del formulario
  const handleSubmit = (evt) => {
    // Actualizar el estado de envío del formulario
    isFormSubmitted === false && setIsFormSubmitted(true);
    // Flag usada para generar un UUID si el formulario es válido
    const generateUUID = true;
    // Validar el formulario y obtener el nuevo estado de los inputs
    const [newInputValues, isFormValid, formData] = onFormSubmitted(
      evt,
      formRef,
      inputValues,
      inventoryInputsObject,
      generateUUID
    );
    // Actualizar el estado de los inputs
    setInputValues(newInputValues);
    // Si el formulario es válido, ejecutar la función para agregar el mobiliario
    isFormValid === true
      ? handleAddMobiliario(formData, formData.id)
      : showErrorAlert("Revisa los datos del formulario.");
  };

  // Función para añaadir un mobiliario al store y el local storage
  const handleAddMobiliario = (formData, id) => {
    // Agregar el mobiliario al store de Redux
    dispatch(addMobiliario(formData));
    // Agregar el mobiliario al local storage
    setLocalStorageData(formData, id);
    // Mostrar una alerta de éxito
    showSuccessAlert("Mobiliario agregado");
  };

  return (
    <section className='contenedor-form-inventario'>
      <form className='formulario' onSubmit={handleSubmit} ref={formRef} noValidate>
        {/* Título del formulario */}
        <h2 className='titulo-seccion'>Agregar mobiliarios</h2>

        {/* Renderizar los inputs del formulario del inventario */}
        {inventoryInputsObject.map((input, index) => (
          <div key={`${input.name}-${index}`}>
            <HtmlFormElement
              inputDetails={input}
              onChange={handleChange}
              isFormSubmitted={isFormSubmitted}
              inputValidityStatus={inputValues[index].isValid}
              initialRenderStatus={inputValues[index].isFirstRender}
              onFirstRenderStatusChange={updateInitialInputRenderFlag}
            />
          </div>
        ))}

        {/* Botón para agregar el mobiliario */}
        <input type='submit' id='btn_inventario' value='Agregar' className='button' />
      </form>
    </section>
  );
};
