import Swal from "sweetalert2";

const useSweetAlert = () => {
  const baseConfig = {
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 1250,
    timerProgressBar: true,
    showClass: { popup: "" },
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  };

  const showSuccessAlert = (message) => {
    Swal.fire({
      ...baseConfig,
      icon: "success",
      title: message,
    });
  };

  const showErrorAlert = (message) => {
    Swal.fire({
      ...baseConfig,
      icon: "error",
      title: message,
    });
  };

  return {
    showSuccessAlert,
    showErrorAlert,
  };
};

export default useSweetAlert;
