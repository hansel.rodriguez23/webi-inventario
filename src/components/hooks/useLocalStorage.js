const useLocalStorage = (key) => {
  const getData = () => {
    const data = localStorage.getItem(key);
    return data ? JSON.parse(data) : [];
  };

  const setData = (data, id = null) => {
    // Si id es null, es porque se van a guardar varios datos
    // Si id no es null, es porque se va a guardar un solo dato
    if (id !== null) {
      // Obtener los datos del local storage
      const dataFromLocalStorage = getData();
      // Agregar el ID al nuevo elemento
      const nuevoElementoConId = { ...data, id };
      // Añadir el nuevo elemento al array
      dataFromLocalStorage.push(nuevoElementoConId);
      // Guardar los datos de los elementos en el localStorage
      localStorage.setItem(key, JSON.stringify(dataFromLocalStorage));
    } else {
      localStorage.setItem(key, JSON.stringify(data));
    }
  };

  return [getData, setData];
};

export default useLocalStorage;
