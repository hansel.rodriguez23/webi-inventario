import React, { useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useDispatch } from "react-redux";

// Componentes
import { Inicio } from "./pages/Inicio";
import { Login } from "./pages/Login";
import { Registro } from "./pages/Registro";
import { Inventario } from "./pages/Inventario";

// Hooks
import useLocalStorage from "./hooks/useLocalStorage";

// Features
import { addUsuario } from "../features/usuarioSlice";

import "../css/index.css";
import "../css/formularios.css";

export const App = () => {
  const dispatch = useDispatch();
  const [getLocalStorageUsersData] = useLocalStorage("usuarios");

  // Obtener los usuarios desde el localStorage
  const usuariosFromLocalStorage = getLocalStorageUsersData();

  // Si el arreglo de usuarios existe en el localStorage, agregarlo al store de Redux
  useEffect(() => {
    if (usuariosFromLocalStorage.length > 0) {
      usuariosFromLocalStorage.forEach((usuario) => {
        dispatch(addUsuario(usuario));
      });
    }
  }, []);

  return (
    <Router>
      <Routes>
        <Route path='/' element={<Inicio />} />
        <Route path='/login' element={<Login />} />
        <Route path='/registro' element={<Registro />} />
        <Route path='/inventario' element={<Inventario />} />
      </Routes>
    </Router>
  );
};
