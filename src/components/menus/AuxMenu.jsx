import React from "react";
import { Link } from "react-router-dom";

import "../../css/auxMenu.css";

export const AuxMenu = () => {
  return (
    <div className='menu-navegacion-aux'>
      <Link to='/' className='menu-navegacion-aux-boton'>
        <i className='fas fa-home menu-navegacion-aux-icon'></i>
        Inicio
      </Link>
      {/* <Link to='/sugerencias' className='menu-navegacion-aux-boton'>
        <i className='fas fa-envelope menu-navegacion-aux-icon'></i>
        Sugerencias
      </Link> */}
    </div>
  );
};

export default AuxMenu;
