import React from "react";
import { Link, useNavigate } from "react-router-dom";

import UserLoggedIcon from "../../assets/user-circle.svg";

// Hooks
import useLocalStorage from "../hooks/useLocalStorage";

import "../../css/UserLoggedInfo.css";

export const TopMenu = ({ activeElement }) => {
  // Propiedades internas del componente
  const navigate = useNavigate();
  const [getLocalStorageData] = useLocalStorage("userLogged");
  const usuario = getLocalStorageData();
  console.log(usuario);

  // Manejar el cierre de la sesión del usuario
  const handleLogOut = () => {
    // Eliminar el usuario del localStorage
    localStorage.removeItem("userLogged");
    // Redireccionar al login
    navigate("/login");
  };

  return (
    <div className='header'>
      <nav>
        <Link to='/'>Inicio</Link>
        <Link className={activeElement == "inventario" ? "active" : ""} to='/inventario'>
          Inventario
        </Link>
        {/* <Link className={activeElement == "sugerencias" ? "active" : ""} to='/sugerencias'>
          Sugerencias
        </Link> */}
      </nav>
      <div className='user-logged-info-container'>
        <img src={UserLoggedIcon} alt='Usuario' className='user-icon' />
        <div className='user-logged-info-data'>
          <span className='user-name'>{usuario[0].username}</span>
          <span className='user-rol'>{usuario[0].rol}</span>
        </div>
        <button className='btn-log-out' onClick={handleLogOut}>
          <i className='fas fa-sign-out-alt log-out-icon'></i>
        </button>
      </div>
    </div>
  );
};
