import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

// Hooks
import useLocalStorage from "../hooks/useLocalStorage";
import useSweetAlert from "../hooks/useSweetAlert";

// Acciones de Redux
import { addMobiliario, deleteMobiliario } from "../../features/mobiliarioSlice";

export const TablaMobiliarios = () => {
  // Propiedades internas del componente
  const dispatch = useDispatch();
  const [getLocalStorageData, setLocalStorageData] = useLocalStorage("mobiliarios");
  const { showSuccessAlert, showErrorAlert } = useSweetAlert();

  // Obtener el estado de los mobiliarios desde el store
  const mobiliarios = useSelector((state) => state.mobiliarios);

  // Obtener los mobiliarios desde el localStorage
  const mobiliariosFromLocalStorage = getLocalStorageData();

  // Si el arreglo de mobiliarios existe en el localStorage, agregarlo al store de Redux
  useEffect(() => {
    if (mobiliariosFromLocalStorage.length > 0) {
      mobiliariosFromLocalStorage.forEach((mobiliario) => {
        dispatch(addMobiliario(mobiliario));
      });
    }
  }, []);

  // Eliminar el mobiliario del store y del localStorage
  const handleDeleteMobiliario = (id) => {
    // Eliminar el mobiliario del store
    dispatch(deleteMobiliario(id));
    // Eliminar el mobiliario del localStorage
    const nuevosMobiliarios = mobiliariosFromLocalStorage.filter(
      (mobiliario) => mobiliario.id !== id
    );
    setLocalStorageData(nuevosMobiliarios);
    // Mostrar alerta de éxito
    showSuccessAlert("Mobiliario eliminado");
  };

  return (
    <section className='contendor-items-inventario diseño-caja'>
      <h2 className='titulo-seccion'>Mobiliarios Existentes</h2>
      <table>
        <thead>
          <tr>
            <th>Nro.</th>
            <th>Nombre</th>
            <th>Serie</th>
            <th>Destino</th>
            <th>Estado</th>
            <th>Opciones</th>
          </tr>
        </thead>
        <tbody>
          {
            // mobiliarios diferente de null y diferente de undefined
            mobiliarios &&
              mobiliarios.map((mobiliario, index) => (
                <tr key={mobiliario.id}>
                  <td>{index + 1}</td>
                  <td>{mobiliario.nombre_mobiliario}</td>
                  <td>{mobiliario.numero_serie}</td>
                  <td>{mobiliario.facultad}</td>
                  <td>{mobiliario.estado}</td>
                  <td>
                    {/* <button id='btn-tabla-editar' className='btn-tabla'>
                      <i className='fas fa-pencil-alt btn-tabla-icon'></i>
                    </button> */}
                    <button
                      id='btn-tabla-eliminar'
                      className='btn-tabla'
                      onClick={() => handleDeleteMobiliario(mobiliario.id)}
                    >
                      <i className='fas fa-trash-alt btn-tabla-icon'></i>
                    </button>
                  </td>
                </tr>
              ))
          }
        </tbody>
      </table>
    </section>
  );
};
