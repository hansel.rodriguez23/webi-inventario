import React, { useState, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

// Components
import AuxMenu from "../menus/AuxMenu";
import HtmlFormElement from "../forms/HtmlFormElement";

// Hooks
import useLocalStorage from "../hooks/useLocalStorage";
import useSweetAlert from "../hooks/useSweetAlert";

// Utils
import {
  onInputChanged,
  onFormSubmitted,
  onInputRenderStatusChange,
} from "../forms/utils/formUtils";

// Constans
import { loginInputsObject } from "../forms/constans/loginInputs";

export const Login = () => {
  // Referencias
  const formRef = useRef(null);
  // Estados
  const usuarios = useSelector((state) => state.usuarios);
  const [getLocalStorageData, setLocalStorageData] = useLocalStorage("userLogged");
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [inputValues, setInputValues] = useState(
    loginInputsObject.map((input) => ({
      name: input.name,
      value: "",
      isValid: false,
      isFirstRender: true,
    }))
  );
  // Hooks
  const navigate = useNavigate();
  const { showSuccessAlert, showErrorAlert } = useSweetAlert();

  const updateInitialInputRenderFlag = (inputName, inputStatus) => {
    // Actualizar el estado de los inputs
    const newInputValues = onInputRenderStatusChange(inputName, inputValues, inputStatus);
    // Actualizar el estado del componente
    setInputValues(newInputValues);
  };

  // Actualizar el estado del componente dependiendo del input que cambió
  const handleChange = (event) => {
    // Actualizar el estado de los inputs
    const newInputValues = onInputChanged(event, inputValues);
    // Actualizar el estado del componente
    setInputValues(newInputValues);
  };

  // Manejar el evento submit del formulario
  const handleSubmit = (evt) => {
    // Actualizar el estado de envío del formulario
    isFormSubmitted === false && setIsFormSubmitted(true);
    // Validar el formulario y obtener el nuevo estado de los inputs
    const [newInputValues, isFormValid, formData] = onFormSubmitted(
      evt,
      formRef,
      inputValues,
      loginInputsObject
    );
    // Actualizar el estado de los inputs
    setInputValues(newInputValues);
    // Si el formulario es válido, enviar los datos al servidor
    isFormValid === true
      ? handleLogin(formData)
      : showErrorAlert("Revisa los datos del formulario");
  };

  // Manejar el inicio de sesión del usuario
  const handleLogin = (formData) => {
    // Obtener los datos del usuario que intenta iniciar sesión
    const { username, password } = formData;
    // Buscar el usuario en el estado de Redux
    const usuario = usuarios.find(
      (usuario) => usuario.username === username && usuario.password === password
    );
    // Si el usuario no existe, mostrar un mensaje de error
    if (!usuario) {
      showErrorAlert("¡Usuario o contraseña incorrectos!");
      return;
    }
    // Si el usuario existe, guardar su información en el LocalStorage
    setLocalStorageData(usuario, usuario.id);
    // Redireccionar al usuario a la página del inventario
    setTimeout(() => navigate("/inventario"), 50)
  };

  return (
    <div className='form-contenedor'>
      <AuxMenu />
      <form id='sesionForm' className='formulario' onSubmit={handleSubmit} ref={formRef} noValidate>
        {/* Título del formulario */}
        <h1 className='titulo-formulario'>Iniciar Sesión</h1>

        {/* Renderizar los inputs del Login */}
        {loginInputsObject.map((input, index) => (
          <div key={`${input.name}-${index}`}>
            <HtmlFormElement
              inputDetails={input}
              onChange={handleChange}
              isFormSubmitted={isFormSubmitted}
              inputValidityStatus={inputValues[index].isValid}
              initialRenderStatus={inputValues[index].isFirstRender}
              onFirstRenderStatusChange={updateInitialInputRenderFlag}
            />
          </div>
        ))}

        {/* Botón para iniciar la sesión */}
        <input type='submit' value='Iniciar Sesión' className='button' />

        {/* Link para ir al formulario de registro */}
        <p className='ir-a'>
          ¿No tienes una cuenta? &nbsp;
          <Link to='/registro' id='register' className='link'>
            Registrate
          </Link>
        </p>
      </form>
    </div>
  );
};
