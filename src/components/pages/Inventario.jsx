import React from "react";

// Componentes
import { TopMenu } from "../menus/TopMenu";
import { FormInventario } from "../forms/FormInventario";
import { TablaMobiliarios } from "../tables/TablaMobiliarios";

import "../../css/inventario.css";

export const Inventario = () => {
  return (
    <>
      <TopMenu activeElement='inventario' />
      <main className='contenedor-inventario'>
        <FormInventario />
        <TablaMobiliarios />
      </main>
    </>
  );
};
