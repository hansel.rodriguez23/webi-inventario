import React, { useState, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

// Components
import HtmlFormElement from "../forms/HtmlFormElement";

// Hooks
import useLocalStorage from "../hooks/useLocalStorage";
import useSweetAlert from "../hooks/useSweetAlert";

// Utils
import {
  onInputChanged,
  onFormSubmitted,
  onInputRenderStatusChange,
} from "../forms/utils/formUtils";

// Constans
import { registerInputsObject } from "../forms/constans/registerInputs";

// Features
import { addUsuario } from "../../features/usuarioSlice";

export const Registro = () => {
  // Referencias
  const formRef = useRef(null);
  // Estados
  const [getLocalStorageData, setLocalStorageData] = useLocalStorage("usuarios");
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [inputValues, setInputValues] = useState(
    registerInputsObject.map((input) => ({
      name: input.name,
      value: "",
      isValid: false,
      isFirstRender: true,
    }))
  );
  // Hooks
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { showSuccessAlert, showErrorAlert } = useSweetAlert();

  const updateInitialInputRenderFlag = (inputName, inputStatus) => {
    // Actualizar el estado de los inputs
    const newInputValues = onInputRenderStatusChange(inputName, inputValues, inputStatus);
    // Actualizar el estado del componente
    setInputValues(newInputValues);
  };

  // Actualizar el estado del componente dependiendo del input que cambió
  const handleChange = (event) => {
    // Actualizar el estado de los inputs
    const newInputValues = onInputChanged(event, inputValues);
    // Actualizar el estado del componente
    setInputValues(newInputValues);
  };

  // Manejar el evento submit del formulario
  const handleSubmit = (evt) => {
    // Actualizar el estado de envío del formulario
    isFormSubmitted === false && setIsFormSubmitted(true);
    // Flag usada para generar un UUID si el formulario es válido
    const generateUUID = true;
    // Validar el formulario y obtener el nuevo estado de los inputs
    const [newInputValues, isFormValid, formData] = onFormSubmitted(
      evt,
      formRef,
      inputValues,
      registerInputsObject,
      generateUUID
    );
    // Actualizar el estado de los inputs
    setInputValues(newInputValues);
    // Si el formulario es válido, enviar los datos al servidor
    isFormValid === true
      ? handleRegister(formData)
      : showErrorAlert("Revisa los datos del formulario");
  };

  // Manejar el registro del usuario
  const handleRegister = (userData) => {
    // Enviar los datos del usuario al estado de Redux
    dispatch(addUsuario(userData));
    // Agregar el usuario al local storage
    setLocalStorageData(userData, userData.id);
    // Mostrar una alerta de éxito
    showSuccessAlert("Usuario registrado");
    // Redireccionar al usuario a la página de login después de 1250 ms
    setTimeout(() => navigate("/login"), 1250);
  };

  return (
    <div className='form-contenedor'>
      <form
        id='registroform'
        className='formulario'
        onSubmit={handleSubmit}
        ref={formRef}
        noValidate
      >
        {/* Título del formulario */}
        <h1 className='titulo-formulario'>Registro de usuario</h1>

        {/* Renderizar los inputs del Registro */}
        {registerInputsObject.map((input, index) => (
          <div key={`${input.name}-${index}`}>
            <HtmlFormElement
              inputDetails={input}
              onChange={handleChange}
              isFormSubmitted={isFormSubmitted}
              inputValidityStatus={inputValues[index].isValid}
              initialRenderStatus={inputValues[index].isFirstRender}
              onFirstRenderStatusChange={updateInitialInputRenderFlag}
            />
          </div>
        ))}

        {/* Botón para realizar el registro */}
        <input type='submit' value='Registrate' className='button' />

        {/* Link para ir al formulario del login */}
        <p className='ir-a'>
          ¿Ya tienes una cuenta? &nbsp;
          <Link to='/login' id='login' className='link'>
            Iniciar Sesión
          </Link>
        </p>
      </form>
    </div>
  );
};
