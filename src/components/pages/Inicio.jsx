import { Link } from "react-router-dom";

// Hooks
import useLocalStorage from "../hooks/useLocalStorage";

import "../../css/inicio.css";

import HomeIcon from "../../assets/home.svg";
import UserIcon from "../../assets/user.svg";
import EmailIcon from "../../assets/email.svg";
import InventoryIcon from "../../assets/inventory.svg";

export const Inicio = () => {
  // Hooks
  const [getLocalStorageData] = useLocalStorage("userLogged");

  // Obtener el usuario que ha iniciado la sesión desde el localStorage
  const userLogged = getLocalStorageData();

  return (
    <div className='container'>
      <nav className='contenedor-menu-lateral'>
        <ul className='menu-lateral-items'>
          {/* <li className=.contenedor-menu-lateral-items"></li> */}

          <Link to='/' id='home' className='menu-lateral-enlace'>
            <img id='home' src={HomeIcon} alt='Inicio' className='menu-lateral-icon' />
            INICIO
          </Link>

          {
            // Si no hay un usuario loggeado, mostrar el enlace para iniciar sesión
            // Si hay un usuario loggeado, mostrar el enlace para ir al inventario
            userLogged.length === 0 ? (
              <Link to='/login' id='login' className='menu-lateral-enlace'>
                <img id='home' src={UserIcon} alt='Iniciar sesión' className='menu-lateral-icon' />
                INICIO DE SESIÓN
              </Link>
            ) : (
              <Link to='/inventario' id='inventario' className='menu-lateral-enlace'>
                <img id='home' src={InventoryIcon} alt='Inventario' className='menu-lateral-icon' />
                INVENTARIO
              </Link>
            )
          }

          {/* <Link to='/sugerencias' id='suggestions' className='menu-lateral-enlace'>
            <img id='home' src={EmailIcon} alt='Sugerencias' className='menu-lateral-icon' />
            SUGERENCIAS
          </Link> */}
        </ul>
      </nav>
    </div>
  );
};
